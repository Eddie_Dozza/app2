﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using App2.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace App2
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<TotalAmount> TotalAmounts { get; set; }
        public DbSet<TotalMonthlyPayment> TotalMonthlyPayments { get; set; }
        public DbSet<ProlongationAmount> ProlongationAmounts { get; set; }
        public DbSet<ContractData> ContractData { get; set; }

        public DbSet<SubjectRole> SubjectRole { get; set; }
        public DbSet<Contract> Contract { get; set; }
        public DbSet<Batch> Batch { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<TotalAmount>()
                .HasData(new TotalAmount()
                    {
                        Value = "20000",
                        Currency = "USD",
                        Id = 1
                    }
                );

            builder.Entity<TotalMonthlyPayment>()
                .HasData(new TotalMonthlyPayment()
                    {
                        Value = "1000",
                        Currency = "USD",
                        Id = 1
                    }
                );

            builder.Entity<ProlongationAmount>()
                .HasData(new ProlongationAmount()
                    {
                        Value = "0",
                        Currency = "USD",
                        Id = 1
                    }
                );

            builder.Entity<ContractData>()
                .HasData(new ContractData()
                    {
                        Branch = "Branch0",
                        PhaseOfContract = "Open",
                        ContractStatus = "GrantedAndActivated",
                        TypeOfContract = "Installment",
                        ContractSubtype = "ConsumerLoan",
                        OwnershipType = "Individual",
                        PurposeOfFinancing = "Construct",
                        CurrencyOfContract = "USD",
                        TotalAmountId = 1,
                        NextPaymentDate = "2018-05-15",
                        TotalMonthlyPaymentId = 1,
                        PaymentPeriodicity = "",
                        StartDate = "2018-01-15",
                        RestructuringDate = "2018-01-15",
                        ExpectedEndDate = "2018-01-15",
                        RealEndDate = "",
                        NegativeStatusOfContract = "",
                        ProlongationAmountId = 1,
                        ProlongationDate = "2018-01-15",
                        Id = 1
                     }
                );

            builder.Entity<SubjectRole>()
                .HasData(new SubjectRole()
                    {
                        CustomerCode = "CustomerCode1",
                        RoleOfCustomer = "Guarantor",
                        RealEndDate = "2018-01-15",
                        GuarantorRelationship = "1",
                        Id = 1,
                        ContractId = 1

                },
                    new SubjectRole()
                    {
                        CustomerCode = "CustomerCode2",
                        RoleOfCustomer = "Guarantor",
                        RealEndDate = "2018-01-15",
                        GuarantorRelationship = "2",
                        Id = 2,
                        ContractId = 1
                    },
                    new SubjectRole()
                    {
                        CustomerCode = "CustomerCode3",
                        RoleOfCustomer = "Guarantor",
                        RealEndDate = "2018-01-15",
                        GuarantorRelationship = "3",
                        Id = 3,
                        ContractId = 1
                    }
                );
            
            builder.Entity<Contract>()
                .HasData(new Contract()
                    {
                        ContractCode = "ContractCode0",
                        ContractDataId = 1,
                        SubjectRole =  new List<SubjectRole>()
                        {
                           

                        },
                        Id = 1
                }
                );

            builder.Entity<Batch>()
                .HasData(new Batch()
                    {
                        Id = 1,
                        BatchIdentifier = "BANK_20180115_1",
                        ContractId = 1
                }
                );
        }
    }
}
