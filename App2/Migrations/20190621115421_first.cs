﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App2.Migrations
{
    public partial class first : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProlongationAmounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProlongationAmounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TotalAmounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TotalAmounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TotalMonthlyPayments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TotalMonthlyPayments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Branch = table.Column<string>(nullable: true),
                    PhaseOfContract = table.Column<string>(nullable: true),
                    ContractStatus = table.Column<string>(nullable: true),
                    TypeOfContract = table.Column<string>(nullable: true),
                    ContractSubtype = table.Column<string>(nullable: true),
                    OwnershipType = table.Column<string>(nullable: true),
                    PurposeOfFinancing = table.Column<string>(nullable: true),
                    CurrencyOfContract = table.Column<string>(nullable: true),
                    TotalAmountId = table.Column<int>(nullable: false),
                    NextPaymentDate = table.Column<string>(nullable: true),
                    TotalMonthlyPaymentId = table.Column<int>(nullable: false),
                    PaymentPeriodicity = table.Column<string>(nullable: true),
                    StartDate = table.Column<string>(nullable: true),
                    RestructuringDate = table.Column<string>(nullable: true),
                    ExpectedEndDate = table.Column<string>(nullable: true),
                    RealEndDate = table.Column<string>(nullable: true),
                    NegativeStatusOfContract = table.Column<string>(nullable: true),
                    ProlongationAmountId = table.Column<int>(nullable: false),
                    ProlongationDate = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContractData_ProlongationAmounts_ProlongationAmountId",
                        column: x => x.ProlongationAmountId,
                        principalTable: "ProlongationAmounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractData_TotalAmounts_TotalAmountId",
                        column: x => x.TotalAmountId,
                        principalTable: "TotalAmounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContractData_TotalMonthlyPayments_TotalMonthlyPaymentId",
                        column: x => x.TotalMonthlyPaymentId,
                        principalTable: "TotalMonthlyPayments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contract",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractCode = table.Column<string>(nullable: true),
                    ContractDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contract_ContractData_ContractDataId",
                        column: x => x.ContractDataId,
                        principalTable: "ContractData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Batch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BatchIdentifier = table.Column<string>(nullable: true),
                    ContractId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Batch", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Batch_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectRole",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerCode = table.Column<string>(nullable: true),
                    RoleOfCustomer = table.Column<string>(nullable: true),
                    RealEndDate = table.Column<string>(nullable: true),
                    GuarantorRelationship = table.Column<string>(nullable: true),
                    ContractId = table.Column<int>(nullable: false),
                    Contract = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubjectRole_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ProlongationAmounts",
                columns: new[] { "Id", "Currency", "Value" },
                values: new object[] { 1, "USD", "0" });

            migrationBuilder.InsertData(
                table: "TotalAmounts",
                columns: new[] { "Id", "Currency", "Value" },
                values: new object[] { 1, "USD", "20000" });

            migrationBuilder.InsertData(
                table: "TotalMonthlyPayments",
                columns: new[] { "Id", "Currency", "Value" },
                values: new object[] { 1, "USD", "1000" });

            migrationBuilder.InsertData(
                table: "ContractData",
                columns: new[] { "Id", "Branch", "ContractStatus", "ContractSubtype", "CurrencyOfContract", "ExpectedEndDate", "NegativeStatusOfContract", "NextPaymentDate", "OwnershipType", "PaymentPeriodicity", "PhaseOfContract", "ProlongationAmountId", "ProlongationDate", "PurposeOfFinancing", "RealEndDate", "RestructuringDate", "StartDate", "TotalAmountId", "TotalMonthlyPaymentId", "TypeOfContract" },
                values: new object[] { 1, "Branch0", "GrantedAndActivated", "ConsumerLoan", "USD", "2018-01-15", "", "2018-05-15", "Individual", "", "Open", 1, "2018-01-15", "Construct", "", "2018-01-15", "2018-01-15", 1, 1, "Installment" });

            migrationBuilder.InsertData(
                table: "Contract",
                columns: new[] { "Id", "ContractCode", "ContractDataId" },
                values: new object[] { 1, "ContractCode0", 1 });

            migrationBuilder.InsertData(
                table: "Batch",
                columns: new[] { "Id", "BatchIdentifier", "ContractId" },
                values: new object[] { 1, "BANK_20180115_1", 1 });

            migrationBuilder.InsertData(
                table: "SubjectRole",
                columns: new[] { "Id", "Contract", "ContractId", "CustomerCode", "GuarantorRelationship", "RealEndDate", "RoleOfCustomer" },
                values: new object[,]
                {
                    { 1, null, 1, "CustomerCode1", "1", "2018-01-15", "Guarantor" },
                    { 2, null, 1, "CustomerCode2", "2", "2018-01-15", "Guarantor" },
                    { 3, null, 1, "CustomerCode3", "3", "2018-01-15", "Guarantor" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Batch_ContractId",
                table: "Batch",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_ContractDataId",
                table: "Contract",
                column: "ContractDataId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractData_ProlongationAmountId",
                table: "ContractData",
                column: "ProlongationAmountId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractData_TotalAmountId",
                table: "ContractData",
                column: "TotalAmountId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractData_TotalMonthlyPaymentId",
                table: "ContractData",
                column: "TotalMonthlyPaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectRole_ContractId",
                table: "SubjectRole",
                column: "ContractId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Batch");

            migrationBuilder.DropTable(
                name: "SubjectRole");

            migrationBuilder.DropTable(
                name: "Contract");

            migrationBuilder.DropTable(
                name: "ContractData");

            migrationBuilder.DropTable(
                name: "ProlongationAmounts");

            migrationBuilder.DropTable(
                name: "TotalAmounts");

            migrationBuilder.DropTable(
                name: "TotalMonthlyPayments");
        }
    }
}
