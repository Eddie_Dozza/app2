﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App2.Models
{
    public class TotalAmount
    {   
        public int Id { get; set; }
        public string Value { get; set; }
        public string Currency { get; set; }
    }

    public class TotalMonthlyPayment
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Currency { get; set; }
    }

    public class ProlongationAmount
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Currency { get; set; }
    }

    public class ContractData
    {
        public int Id { get; set; }
        public string Branch { get; set; }
        public string PhaseOfContract { get; set; }
        public string ContractStatus { get; set; }
        public string TypeOfContract { get; set; }
        public string ContractSubtype { get; set; }
        public string OwnershipType { get; set; }
        public string PurposeOfFinancing { get; set; }
        public string CurrencyOfContract { get; set; }

        public int TotalAmountId { get; set; }
        public TotalAmount TotalAmount { get; set; }
        public string NextPaymentDate { get; set; }

        public int TotalMonthlyPaymentId { get; set; }
        public TotalMonthlyPayment TotalMonthlyPayment { get; set; }
        public string PaymentPeriodicity { get; set; }
        public string StartDate { get; set; }
        public string RestructuringDate { get; set; }
        public string ExpectedEndDate { get; set; }
        public string RealEndDate { get; set; }
        public string NegativeStatusOfContract { get; set; }

        public int ProlongationAmountId { get; set; }
        public ProlongationAmount ProlongationAmount { get; set; }
        public string ProlongationDate { get; set; }
    }

    public class SubjectRole
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public string RoleOfCustomer { get; set; }
        public string RealEndDate { get; set; }
        public string GuarantorRelationship { get; set; }
        public int ContractId { get; set; }
        public string Contract { get; set; }

    }

    public class Contract
    {
        public int Id { get; set; }
        public string ContractCode { get; set; }
        public int ContractDataId { get; set; }
        public ContractData ContractData { get; set; }
        public List<SubjectRole> SubjectRole { get; set; }
    }

    public class Batch
    {
        public int Id { get; set; }
        public string BatchIdentifier { get; set; }

        public int ContractId { get; set; }
        public Contract Contract { get; set; }
    }

    //public class RootObject
    //{
    //    public Batch Batch { get; set; }
    //}
}
