﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using App2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public ValuesController(ApplicationDbContext context)
        {
            this.context = context;
        }
        // GET api/values
        [HttpGet]
        public XDocument Get()
        {


            // сделал codefirst, нужно накатить миграцию.
            //тут дастаем нужный нам batch
            Batch batch = context.Batch.Include(x => x.Contract).Include(x => x.Contract.ContractData)
                .Include(x => x.Contract.ContractData.ProlongationAmount).Include(x => x.Contract.ContractData.TotalAmount)
                .Include(x => x.Contract.ContractData.TotalMonthlyPayment).Include(x => x.Contract.SubjectRole)
                .FirstOrDefault(x => x.Id == 1);
              // собераем xml
            XDocument xdoc = new XDocument(new XElement("Batch",
                new XElement("BatchIdentifier", batch.BatchIdentifier),
                new XElement("ContractCode", batch.Contract.ContractCode),
                new XElement("ContractData",
                    new XElement("Branch", batch.Contract.ContractData.Branch),
                    new XElement("PhaseOfContract", batch.Contract.ContractData.PhaseOfContract),
                    new XElement("ContractStatus", batch.Contract.ContractData.ContractStatus),
                    new XElement("TypeOfContract", batch.Contract.ContractData.TypeOfContract),
                    new XElement("ContractSubtype", batch.Contract.ContractData.ContractSubtype),
                    new XElement("OwnershipType", batch.Contract.ContractData.OwnershipType),
                    new XElement("PurposeOfFinancing", batch.Contract.ContractData.PurposeOfFinancing),
                    new XElement("CurrencyOfContract", batch.Contract.ContractData.CurrencyOfContract),
                    new XElement("TotalAmount", new XElement("Value", batch.Contract.ContractData.TotalAmount.Value),
                        new XElement("Currency", batch.Contract.ContractData.TotalAmount.Currency)),
                    new XElement("NextPaymentDate", batch.Contract.ContractData.NextPaymentDate),
                    new XElement("TotalMonthlyPayment", new XElement("Value", batch.Contract.ContractData.TotalMonthlyPayment.Value),
                        new XElement("Currency", batch.Contract.ContractData.TotalMonthlyPayment.Currency)),
                    new XElement("StartDate", batch.Contract.ContractData.StartDate),
                    new XElement("RestructuringDate", batch.Contract.ContractData.RestructuringDate),
                    new XElement("ExpectedEndDate", batch.Contract.ContractData.ExpectedEndDate),
                    new XElement("RestructuringDate", batch.Contract.ContractData.RestructuringDate),
                    new XElement("ProlongationAmount", new XElement("Value", batch.Contract.ContractData.ProlongationAmount.Value),
                        new XElement("Currency", batch.Contract.ContractData.ProlongationAmount.Currency)),
                    new XElement("ProlongationDate", batch.Contract.ContractData.ProlongationDate)),
                new XElement("SubjectRole", new XElement("CustomerCode", batch.Contract.SubjectRole.First(x => x.Id == 1).CustomerCode),
                    new XElement("RoleOfCustomer", batch.Contract.SubjectRole.First(x => x.Id == 1).RoleOfCustomer),
                    new XElement("RealEndDate", batch.Contract.SubjectRole.First(x => x.Id == 1).RealEndDate),
                    new XElement("GuarantorRelationship", batch.Contract.SubjectRole.First(x => x.Id == 1).GuarantorRelationship)),
                new XElement("SubjectRole", new XElement("CustomerCode", batch.Contract.SubjectRole.First(x => x.Id == 2).CustomerCode),
                    new XElement("RoleOfCustomer", batch.Contract.SubjectRole.First(x => x.Id == 2).RoleOfCustomer),
                    new XElement("RealEndDate", batch.Contract.SubjectRole.First(x => x.Id == 2).RealEndDate),
                    new XElement("GuarantorRelationship", batch.Contract.SubjectRole.First(x => x.Id == 2).GuarantorRelationship)),
                new XElement("SubjectRole", new XElement("CustomerCode", batch.Contract.SubjectRole.First(x => x.Id == 3).CustomerCode),
                    new XElement("RoleOfCustomer", batch.Contract.SubjectRole.First(x => x.Id == 3).RoleOfCustomer),
                    new XElement("RealEndDate", batch.Contract.SubjectRole.First(x => x.Id == 3).RealEndDate),
                    new XElement("GuarantorRelationship", batch.Contract.SubjectRole.First(x => x.Id == 3).GuarantorRelationship))

            ));
              // парсим номер
            string parse = batch.BatchIdentifier[batch.BatchIdentifier.Length - 1].ToString();
            int index = int.Parse(parse);
            //сохраняем сам файл
            xdoc.Save($"BANK_20180115_{index}.xml");
            index++;
            batch.BatchIdentifier = $"BANK_20180115_{index}";
            //сохранаем изменения номера в бд
            context.Update(batch);
            context.SaveChanges();
        

            return xdoc;
        }

        
    }
}
